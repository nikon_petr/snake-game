//******Классы игровых объектов******
//***********************************
function ColorStyle(){
    this.setStyle = function(c){
        switch(c){
            case 'green':
                this.hexagonColor = "rgba(0, 0, 0, 0.6)";//черный
                this.borderColor = "rgba(31, 119, 229, 0.6)";
                this.snakeColor = "rgba(229, 217, 31, 1)";
                this.backgroundColor = "rgba(49, 189, 49, 0.6)"; 
                break;
            case 'grey':
                this.hexagonColor = "rgba(255, 180, 0, 0.6)";
                this.borderColor = "rgba(31, 119, 229, 0.6)";
                this.snakeColor = "black";
                this.backgroundColor = "rgba(150, 150, 150, 0.6)"; 
                break;
            case 'blue':
                this.hexagonColor = "rgba(150, 150, 150, 0.9)";
                this.borderColor = "red";
                this.snakeColor = "rgba(229, 217, 31, 1)";
                this.backgroundColor = "rgba(31, 119, 229, 0.6)"; 
                break;
        }
    };     
    this.updateStyle = function(windowback, map, snake, food){
        windowback.canvas.width = windowback.canvas.width;
        windowback.DrawBackground();
        map.updateMap(snake);
        $("#leftMenu").css('backgroundColor', this.hexagonColor);
        $("#rightMenu").css('backgroundColor', this.hexagonColor);
    };
}

function Background(colorStyle){
    this.Width = $(document).width();
    this.Height = $(document).height();
    $("body").prepend("<canvas id='Background' width='" + this.Width + "' height='" + this.Height + "'></canvas>");
    $("#Background").css('z-index', '-1');
    this.canvas = $("#Background").get(0);
    this.background = this.canvas.getContext("2d");
    this.DrawBackground = function(){
        this.background.fillStyle = colorStyle.backgroundColor;
        this.background.fillRect(0, 0, this.Width, this.Height); 
        //1
        this.background.beginPath();
        this.background.moveTo(0, this.Height);
        this.background.lineTo(0, this.Height * 0.9);
        this.background.lineTo(this.Width * 0.1, 0);
        this.background.lineTo(this.Width, 0);
        this.background.lineTo(this.Width, this.Height);
        this.background.closePath();
        this.background.fill();
        //2
        this.background.beginPath();
        this.background.moveTo(0, this.Height);
        this.background.lineTo(0, this.Height * 0.9);
        this.background.lineTo(this.Width * 0.5, 0);
        this.background.lineTo(this.Width, 0);
        this.background.lineTo(this.Width, this.Height);
        this.background.closePath();
        this.background.fill();
        //3
        this.background.beginPath();
        this.background.moveTo(0, this.Height);
        this.background.lineTo(0, this.Height * 0.9);
        this.background.lineTo(this.Width * 0.9, 0);
        this.background.lineTo(this.Width, 0);
        this.background.lineTo(this.Width, this.Height);
        this.background.closePath();
        this.background.fill();
        //4
        this.background.beginPath();
        this.background.moveTo(0, this.Height);
        this.background.lineTo(0, this.Height * 0.9);
        this.background.lineTo(this.Width, this.Height * 0.6);
        this.background.lineTo(this.Width, this.Height);
        this.background.closePath();
        this.background.fill();
    }
};

//Карта
function Map(colorStyle, level){
    $("#GameWindow").css('z-index', '10');
    this.canvas = $("#GameWindow").get(0);
    this.frame = this.canvas.getContext("2d");
    
    this.Width = $("#GameWindow").width();
    this.Height = $("#GameWindow").height();   
    this.level = level;
    this.size = 0;
    this.points = new Array();
    for(var i = 20, iy = 0; i < this.Height - 30; i += 20 * Math.sqrt(3), iy++){
        this.points.push(new Array());
        for(var j = 20, ix = 0; j < this.Width - 20; j += 20, ix += 2){
            this.points[iy][ix] = new coordinateStruct(j, i);
        }
        this.points[iy][this.points.length] = 0;
        iy++;
        this.points.push(new Array());
        this.points[iy][0] = 0;
        for(var j = 20 + 10, ix = 1; j < this.Width - 20; j += 20, ix+=2){
            this.points[iy][ix] = new coordinateStruct(j, i + 10 * Math.sqrt(3));
        }
        this.size+=2;
    }

    this.DrawMap = function(){
        this.frame.fillStyle = colorStyle.hexagonColor;
        for(var i = 20; i < this.Height - 30; i += 20 * Math.sqrt(3)){
            for(var j = 20; j < this.Width - 20; j += 20){
                drawHexagon(this.frame, j, i);
            }
            iy++;
            this.points[iy + 1] = [];
            for(var j = 20 + 10; j < this.Width - 20; j += 20){
                drawHexagon(this.frame, j, i + 10 * Math.sqrt(3));
            }
        }
    }
    
    this.updateMap = function(snake, food){
        this.canvas.width = this.canvas.width;
        this.DrawMap();
        snake.DrawSnake();
        food.DrawFood();
    };
}

//Змейка
function Snake(colorStyle, map, length, speed){
    this.bal = 0;
    this.length = length;
    this.speed = speed;
    this.vector = [
        new coordinateStruct(-1, -1),
        new coordinateStruct(1, -1),
        new coordinateStruct(2, 0),
        new coordinateStruct(1, 1),
        new coordinateStruct(-1, 1),
        new coordinateStruct(-2, 0)
    ];
    this.vectorIndex = 2;
    this.body = new Array();
    for(var i = 0, j = 0; i < this.length; i++, j += 2){
        this.body.push(new coordinateStruct(17 - j, 21));
    }
    
    this.DrawSnake = function(){
        map.frame.fillStyle = colorStyle.snakeColor;
        for(var i = 0; i < this.length; i++){
            drawHexagon(map.frame, map.points[this.body[i].y][this.body[i].x].x, map.points[this.body[i].y][this.body[i].x].y);
        }
    };
    
    this.SnakeMove = function(food, map){
        var tailX = this.body[this.body.length - 1].x;
        var tailY = this.body[this.body.length - 1].y;
        for(var i = this.body.length - 1; i > 0; i--){
            this.body[i].x = this.body[i - 1].x;
            this.body[i].y = this.body[i - 1].y;
        }
        if(this.body[0].x + this.vector[this.vectorIndex].x < 66 && 
                this.body[0].x + this.vector[this.vectorIndex].x > -1 &&
                this.body[0].y + this.vector[this.vectorIndex].y < 38 && 
                this.body[0].y + this.vector[this.vectorIndex].y > -1){
            this.body[0].x += this.vector[this.vectorIndex].x;
            this.body[0].y += this.vector[this.vectorIndex].y;
            for(var i = 1; i < this.body.length; i++){
                if(this.body[i].x === this.body[0].x && this.body[i].y === this.body[0].y){
                    this.body[0].x += this.vector[this.vectorIndex].x;
                    if(food.mod){
                        clearTimeout(food.timer);
                        $("#timeProgress").stop();
                        $("#timeProgress").css("width", "0");
                    }
                    clearInterval(interval);
                    $("#baner").css("backgroundColor", colorStyle.hexagonColor);
                    $("#record").text("Ваш счет: " + this.bal);
                    $("#baner").fadeIn();
                    setTimeout(function(){$("#baner").fadeOut();}, 5000);  
                    map = new Map(colorStyle, 0);
                    map.updateMap();
                    $("#bal").text("Ваш счет: " + 0);
                }
            }
            if(food.x === this.body[0].x && food.y === this.body[0].y){
                if(food.mod){
                    clearTimeout(food.timer);
                    $("#timeProgress").stop();
                    $("#timeProgress").css("width", "0");
                }
                clearTimeout(food.timer);
                this.bal++;
                $("#bal").text("Ваш счет: " + this.bal);
                this.body.push(new coordinateStruct(tailX, tailY));
                this.length++;
                food.RandomFood();
                map.updateMap(snake, food);
            }
        }
        else{
           this.body[0].x += this.vector[this.vectorIndex].x;
            if(food.mod){
                clearTimeout(food.timer);
                $("#timeProgress").stop();
                $("#timeProgress").css("width", "0");
            }
           clearInterval(interval);
           $("#baner").css("backgroundColor", colorStyle.hexagonColor);
           $("#record").text("Ваш счет: " + this.bal);
           $("#baner").fadeIn();
           setTimeout(function(){$("#baner").fadeOut();}, 5000);
           map = new Map(colorStyle, 0);
           map.updateMap();
           $("#bal").text("Ваш счет: " + 0);
        }
        map.updateMap(this, food);
    };
}

function Food(colorStyle, map, snake){
    this.x = -1;
    this.y = -1;
    this.mod;
    
    this.RandomFood = function(){
        this.y = Math.floor(Math.random() * (35 - 3 + 1)) + 3;
        if(this.y % 2 === 0){
            this.x = Math.floor(Math.random() * (62 - 3 + 1)) + 3;
            while(this.x % 2 !== 0){
                this.x--;
            }
            console.log(this.x);
        }
        else{
           this.x = Math.floor(Math.random() * (62 - 3 + 1)) + 3;
            while(this.x % 2 === 0){
                this.x--;
            } 
        }
        if(this.mod){
            $("#timeProgress").css("visibility", "visible");
            $("#timeProgress").animate({width: "700px"}, 10000);
            this.timer = setTimeout(function(){
                this.check = snake.bal;
                if(snake.bal === this.check){
                    clearTimeout(food.timer);
                    $("#timeProgress").stop();
                    $("#timeProgress").css("width", "0");
                    clearInterval(interval);
                    $("#baner").css("backgroundColor", colorStyle.hexagonColor);
                    $("#record").text("Ваш счет: " + snake.bal);
                    $("#baner").fadeIn();
                    setTimeout(function(){$("#baner").fadeOut();}, 5000);  
                    map = new Map(colorStyle, 0);
                    map.updateMap();
                    $("#bal").text("Ваш счет: " + 0);
                }
            }, 10000);
        }
    };
    
    this.DrawFood = function(){
        map.frame.fillStyle = colorStyle.borderColor;
        drawHexagon(map.frame, map.points[this.y][this.x].x, map.points[this.y][this.x].y);
    };
}

//*****Вспомогательные классы******
//*********************************
//Рисует шестиугольник
function drawHexagon(frame, x, y){  
    A = 10;//Сторона шестиугольника
    hx = x;
    hy = y - A;
    frame.beginPath();
    frame.moveTo(hx, hy);
    for(var i = 5 * Math.PI / 6; i < 5 * Math.PI / 2; i += Math.PI/3){
        hx = -A * Math.cos(i);
        hy = -A * Math.sin(i);
        frame.lineTo(x + hx, y + hy);
    }
    frame.closePath();
    frame.fill();
}

//Координаты какой-либо точки
function coordinateStruct(x, y){
    this.x = x;
    this.y = y;
}