var speed = 150;
var colorStyle = new ColorStyle();
var windowback = new Background(colorStyle);
var map = new Map(colorStyle, 0);
var snake = new Snake(colorStyle, map, 5, speed);
var food = new Food(colorStyle, map, snake);
var interval;
var superr = false;

$(document).ready(function(){
    $(document).keydown(function(e){
        switch(e.which){
            case 37:
                if(snake.vectorIndex > 0) snake.vectorIndex--;
                else snake.vectorIndex = snake.vector.length - 1;
                break;
            case 39:
                if(snake.vectorIndex < snake.vector.length - 1) snake.vectorIndex++;
                else snake.vectorIndex = 0;
                break;
            case 27:
                $("#stop").click();
                break;
        } 
    });
});
$(document).ready(function(){     
    colorStyle.setStyle('green');
    windowback.DrawBackground();
    map.DrawMap();
    snake.DrawSnake();
});
$(document).ready(function(){
    $("#rightMenu").height($(document).height());
    $("#rightMenu").css('backgroundColor', colorStyle.hexagonColor);
    $("#rightMenu").hover(
        function(){
            $(".contentR").css("display", "block");
            $(".contentR").animate(
                {
                   width: "230px"               
                }, 300
            );
        },
        function(){
            $(".contentR").animate(
                {
                   width: "0px"               
                }, 300, function(){
                    $(".contentR").css("display", "none");
                }
            );
        }
    );
    $("#leftMenu").height($(document).height());
    $("#leftMenu").css('backgroundColor', colorStyle.hexagonColor);
    $("#leftMenu").hover(
        function(){
            $(".contentL").css("display", "block");
            $(".contentL").animate(
                {
                   width: "230px"               
                }, 300
            );
        },
        function(){
            $(".contentL").animate(
                {
                   width: "0px"               
                }, 300, function(){
                    $(".contentL").css("display", "none");
                }
            );
        }
    );
    $("#green").click(function(){
        colorStyle.setStyle('green');
        colorStyle.updateStyle(windowback, map, snake, food);
    });
    $("#grey").click(function(){
        colorStyle.setStyle('grey');
        colorStyle.updateStyle(windowback, map, snake, food);
    });
    $("#blue").click(function(){
        colorStyle.setStyle('blue');
        colorStyle.updateStyle(windowback, map, snake, food);
    });
});
$(document).ready(function(){
    $("#fspeed").click(function(){
        speed = 100;
        $("#slo").text("Уровень сложности: " + "тяжелый");
    });
    $("#mspeed").click(function(){
        speed = 150;
        $("#slo").text("Уровень сложности: " + "средний");
    });
    $("#lspeed").click(function(){
        speed = 220;
        $("#slo").text("Уровень сложности: " + "легкий");
    });
    $("#rstart").click(function(){
        clearInterval(interval);
        map = new Map(colorStyle, 0);
        snake = new Snake(colorStyle, map, 5, speed);
        food = new Food(colorStyle, map, snake);
        food.RandomFood();
        map.updateMap(snake, food);
        runF();
    });
    $("#stop").click(function(){
        clearInterval(interval);
        if(food.mod){
            clearTimeout(food.timer);
            $("#timeProgress").stop();
            $("#timeProgress").css("width", "0");
        }
        map = new Map(colorStyle, 0);
        snake = new Snake(colorStyle, map, 5, speed);
        food = new Food(colorStyle, map, snake);
        map.updateMap(snake, food);
    });
    $("#super").click(function(){
        clearInterval(interval);
        map = new Map(colorStyle, 0);
        snake = new Snake(colorStyle, map, 5, speed);
        food = new Food(colorStyle, map, snake);
        food.mod = true;
        food.RandomFood();
        map.updateMap(snake, food);
        runF();
    });
});

function runF(){
    interval = setInterval(function(){
        snake.DrawSnake();
        snake.SnakeMove(food, map);
    }, speed);
}
