# Snake Game
Implementation of snake game

## Built with
* javascript
* jquery
* html
* css

## Features
* normal mode
* time mode
* three speed modes
* three color themes

![Snake Game](https://bitbucket.org/nikon_petr/snake-game/raw/e76498e6436e53babccc95baf33b32195e49848d/screen.png)